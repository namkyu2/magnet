#include "PrototypeScene.h"
#include "Ball.h"
#include "Ground.h"
#include "PullPush.h"

using namespace cocos2d;

class PrototypeLayer : public Layer
{
public:
    CREATE_FUNC(PrototypeLayer);

    Ball* ball;
    PullPush* pullpush;
    PullPush* pullpush2;
    Ground* ground1;
    Ground* ground2;
    boolean rightpress;
    boolean leftpress;
    CCArray* pullpusharray;
    CCArray* groundarray;
    MenuItem* pMenuItem1;
    MenuItem* pMenuItem2;
    Menu* pMenu;
    Size winSize;
 
    virtual bool init() override
    {
        if(!Layer::init()) return false;

        pullpusharray = CCArray::create();
        groundarray = CCArray::create();
        pullpusharray -> retain();
        groundarray -> retain();

        pMenuItem1 = MenuItemImage::create(
        "btn-play-normal.png", 
        "btn-play-selected.png", 
        this, 
        menu_selector(PrototypeLayer::menucallfirst));

        pMenuItem2 = MenuItemImage::create(
        "btn-about-normal.png",
        "btn-about-selected.png",
        this,
        menu_selector(PrototypeLayer::menucallfirst));

        pMenu = Menu::create(pMenuItem1, pMenuItem2, NULL);
        pMenu->alignItemsVertically();
        this->addChild(pMenu, 2);

        auto listener = EventListenerKeyboard::create();
        listener->onKeyPressed = CC_CALLBACK_2(PrototypeLayer::onKeyPressed, this);
        listener->onKeyReleased = CC_CALLBACK_2(PrototypeLayer::onKeyReleased, this);
    
        _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

        return true;
    }

    void PrototypeLayer::menucallfirst(CCObject *pSender)
    {
        removeChild(pMenu, true);

        ball = Ball::create(12.0f);
        ball->setPosition(Point(150, 15));
        addChild(ball);

        pullpush = PullPush::create(14.0f, 120.0f, 2);
        pullpush -> setPosition (Point(350, 120));
        addChild(pullpush);
        pullpusharray -> addObject(pullpush);

        pullpush = PullPush::create(14.0f, 120.0f, 1);
        pullpush -> setPosition (Point(650, 120));
        addChild(pullpush);
        pullpusharray -> addObject(pullpush);

        addChild(ground1 = Ground::create(Rect(0, 0, 300, 15)));
        addChild(ground2 = Ground::create(Rect(900, 0, 300, 15)));
        groundarray -> addObject(ground1);
        groundarray -> addObject(ground2);

        rightpress = false;
        leftpress = false;

        scheduleUpdate();
        winSize = Director::getInstance()->getWinSize();
    }

    void PrototypeLayer::menucallSecond(CCObject *pSender)
    {
        exit(0);
    }

    void PrototypeLayer::menucallthird(CCObject *pSender)
    {
        exit(0);
    }

    virtual void update(float deltaTime) override
    {
        //attached라는 변수로 붙었나 안붙었나 체크 만약에 pullpush에 붙었으면 힘을 작용안하는 방식으로 간다.
        if ((ball->getattached()) == 0)
        {
            ball->initForce();
            ball->applyForce(Point(0, -6.8f));
            ball->pushforce(ball,pullpusharray);
        }

        if (rightpress == true)
        {
            if ((ball->getattached()) == 0)
            {
                ball->sameVelocity(1);
            }
            if ((ball->getattached()) == 1)
            {
                ball->sameVelocity(1);
            }
        }
        
        if (leftpress == true)
        {
            if ((ball->getattached()) == 0)
            {
                ball->sameVelocity(-1);
            }
            if ((ball->getattached()) == 1)
            {
				ball->sameVelocity(-1);
            }
        }

        ball->updatePhysics(ball, groundarray, pullpusharray, deltaTime);
        Layer::update(deltaTime);
    }

    void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
    {
	    if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	    {
		    rightpress = true;
	    }

	    if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	    {
		    leftpress = true;
	    }


	    if (keyCode == EventKeyboard::KeyCode::KEY_R)
	    {
		    ball->setPosition(Point(20,50));
            ball->resetVelocity();
			ball->setattached(0);
	    }

        if (keyCode == EventKeyboard::KeyCode::KEY_Z)
        {
			change(pullpusharray);
			ball->setattached(0);
        }

    }

    void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
    {
	    if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	    {
		    rightpress = false;
            ball->XresetVelocity();
	    }

	    if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	    {
		    leftpress = false;
            ball->XresetVelocity();
	    }
    }

    void change(CCArray* pparray)		// State 변경
    {
        CCObject* obj = NULL;

    CCARRAY_FOREACH(pparray, obj)
    {
        PullPush* pp = (PullPush*)obj;
        if(pp->able)
        {
		    pp->push->setVisible(true);
		    pp->pull->setVisible(false);
		    pp->able = false;
	    } 
        else
        {
		    pp->push->setVisible(false);
		    pp->pull->setVisible(true);
		    pp->able = true;
	    }
    }
}
    
};

//---------------------------------------------------------------------------
bool PrototypeScene::init()
{
    if(!Scene::init()) return false;
    addChild(PrototypeLayer::create());
    return true;
}