#include "Ground.h"
using namespace cocos2d;

bool Ground::init(const Rect& area)
{
    if(!Node::init()) return false;

    sprite = Sprite::create("ground.png");
    sprite->setPosition(Point::ZERO);
    sprite->setAnchorPoint(Point::ZERO);
    sprite->setScaleX(area.size.width / sprite->getContentSize().width);
    sprite->setScaleY(area.size.height / sprite->getContentSize().height);
    addChild(sprite);

    setContentSize(area.size);
    setPosition(area.origin);

    return true;
}

Ground* Ground::create(const Rect& area)
{
    Ground* ret = new Ground();
    if((ret != nullptr) && ret->init(area))
    {
        ret->autorelease();
    }
    else
        CC_SAFE_RELEASE_NULL(ret);
    return ret;
}

Rect Ground::getArea() const
{
    return Rect(getPositionX(), getPositionY(), getContentSize().width, getContentSize().height);
}
