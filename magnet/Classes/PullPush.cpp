#include "PullPush.h"
using namespace cocos2d;

bool PullPush::init(float radius, float rangeradius)
{
    able = true;

    push = Sprite::create("push.png");
    pull = Sprite::create("pull.png");
    range = Sprite::create("range.png");

    float size = push->getContentSize().width;
    float rangesize = range->getContentSize().width;
    float scale = radius / size * 2;
    float rangescale = rangeradius / rangesize * 2;
    range_scale = rangescale;

    push->setScale(scale);
    pull->setScale(scale);
    range->setScale(rangescale);

    push->setPosition(Point::ZERO);
    pull->setPosition(Point::ZERO);
    range->setPosition(Point::ZERO);

    addChild(push);
    addChild(pull);
    addChild(range);

    setContentSize(push->getContentSize() * scale);

    return true;
}

PullPush* PullPush::create(float radius, float rangeradius, int visible)
{
    PullPush* ret = new PullPush();
    if((ret != nullptr) && ret->init(radius, rangeradius))
        ret->autorelease();
    else
        CC_SAFE_RELEASE_NULL(ret);

    if (visible == 1)
    {
        ret->push->setVisible(false);
        ret->pull->setVisible(true);
        ret->able = true;
    }

    if (visible == 2)
    {
        ret->push->setVisible(true);
        ret->pull->setVisible(false);
        ret->able = false;
    }
	ret->radius = radius;
    ret->range_radius = rangeradius;

    return ret;
}

float PullPush::getRadius() const
{
    return radius;
}

cocos2d::Point PullPush::getrangepostion() const
{
    return range->getPosition();
}

cocos2d::Size PullPush::getrangesize() const
{
    return range->getContentSize() * range_scale;
}

int PullPush::getState() const			// State �θ�
{
	if(able)
		return 1;
	else
		return -1;
}