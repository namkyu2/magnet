#pragma once

#include "cocos2d.h"

class PullPush : public cocos2d::Node
{
public:
    virtual bool init(float radius, float rangeradius); 
    static PullPush* create(float radius, float rangeradius, int visible);

    float getRadius() const;
    cocos2d::Point getrangepostion() const;
    cocos2d::Size getrangesize() const;
	int getState() const;
    cocos2d::Sprite* push;
    cocos2d::Sprite* pull;
    cocos2d::Sprite* range;
    boolean able;
    
    // state 찾기 위해서.

private:
    float radius;
    float range_radius;
    float range_scale;
    

};