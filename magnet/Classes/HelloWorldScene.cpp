#include "HelloWorldScene.h"

USING_NS_CC;

class HelloWorldLayer : public cocos2d::Layer
{
public:
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();  
    
    // a selector callback
    void menuCloseCallback(Object* pSender);
    

    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorldLayer);

	virtual void update(float deltaTime) override;
	
    virtual void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) override;
	virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event) override;
	
	cocos2d::Point origin;

	cocos2d::Sprite* plusball;
	cocos2d::Sprite* minusball;
	cocos2d::Sprite* ground;
	cocos2d::Sprite* ground2;

	cocos2d::Sprite* obstacle1;

	bool rightpress; //right keyboard press
	bool leftpress; //left keyboard press
	bool plus_enable;

private:
	cocos2d::Sprite* ball;
	cocos2d::Point currentposition;
	cocos2d::Point temp_groundposition;
	cocos2d::Point temp_groundsize;
	cocos2d::Point temp_ballposition;
	cocos2d::Point temp_ballsize;
};

// on "init" you need to initialize your instance
bool HelloWorldLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();
   

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorldLayer::menuCloseCallback, this));
    
	closeItem->setPosition(Point(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Point::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label

    // add "HelloWorld" splash screen"
    ground = Sprite::create("ground.png");
    ground->setPosition(Point(visibleSize.width/4 + origin.x, origin.y));
    this->addChild(ground, 0);

	ground2 = Sprite::create("ground2.png");
	ground2->setPosition(Point(origin.x + 880, origin.y));
	this->addChild(ground2, 0);

	plusball = Sprite::create("plus_ball.png");
	plusball->setPosition(Point(origin.x + 40 , 60 + origin.y));
	currentposition = plusball->getPosition();
	this->addChild(plusball, 0);

	minusball = Sprite::create("minus_ball.png");
	minusball->setPosition(Point(origin.x + 40 , 60 + origin.y));
	currentposition = minusball->getPosition();
	this->addChild(minusball, 0);

	obstacle1 = Sprite::create("obstacle1.png");
	obstacle1->setPosition(origin.x + 550, origin.y + 140);
	this->addChild(obstacle1, 0);

	plus_enable = true;
	minusball->setVisible(false);

	scheduleUpdate();

	auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(HelloWorldLayer::onKeyPressed, this);
    listener->onKeyReleased = CC_CALLBACK_2(HelloWorldLayer::onKeyReleased, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    return true;
}


void HelloWorldLayer::menuCloseCallback(Object* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

void HelloWorldLayer::update(float deltaTime)
{
	
	temp_groundposition = ground->getPosition();
	temp_groundsize = ground->getContentSize();

	if (plus_enable == true)
	{
		temp_ballposition = plusball->getPosition();
		temp_ballsize = plusball->getContentSize();

		if ((temp_ballposition.y - temp_groundposition.y) > (temp_ballsize.y/2 + temp_groundsize.y/2))
		{
			plusball->setPosition(currentposition.x ,currentposition.y - deltaTime*180);
			currentposition = plusball->getPosition();
			minusball->setPosition(currentposition);
		}

		if ((temp_ballposition.x - temp_groundposition.x) > (temp_groundsize.x/2))
		{
			plusball->setPosition(currentposition.x + deltaTime*140 ,currentposition.y - deltaTime*180);
			currentposition = plusball->getPosition();
			minusball->setPosition(currentposition);
		}
		
	}

	else
	{
		temp_ballposition = minusball->getPosition();
		temp_ballsize = minusball->getContentSize();

		if ((temp_ballposition.y - temp_groundposition.y) > (temp_ballsize.y/2 + temp_groundsize.y/2))
		{
			minusball->setPosition(currentposition.x ,currentposition.y - deltaTime*180);
			currentposition = minusball->getPosition();
			plusball->setPosition(currentposition);
		}

		if ((temp_ballposition.x - temp_groundposition.x) > (temp_groundsize.x/2))
		{
			minusball->setPosition(currentposition.x + deltaTime*140 ,currentposition.y - deltaTime*180);
			currentposition = minusball->getPosition();
			plusball->setPosition(currentposition);
		}
	}

	if (rightpress == true)
	{
		if (plus_enable == true)
		{
			plusball->setPosition(currentposition.x + deltaTime*140,currentposition.y);
			currentposition = plusball->getPosition();
			minusball->setPosition(currentposition);
		}

		else
		{
			minusball->setPosition(currentposition.x + deltaTime*140,currentposition.y);
			currentposition = minusball->getPosition();
			plusball->setPosition(currentposition);
		}
	}

	if (leftpress == true)
	{
		if (plus_enable == true)
		{
			plusball->setPosition(currentposition.x - deltaTime*140,currentposition.y);
			currentposition = plusball->getPosition();
			minusball->setPosition(currentposition);
		}

		else
		{
			minusball->setPosition(currentposition.x - deltaTime*140,currentposition.y);
			currentposition = minusball->getPosition();
			plusball->setPosition(currentposition);
		}
	}



	
}


void HelloWorldLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	{
		rightpress = true;
	}

	if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	{
		leftpress = true;
	}

	if (keyCode == EventKeyboard::KeyCode::KEY_Z)
	{
		if (plus_enable == true)
		{
			plusball->setVisible(false);
			minusball->setVisible(true);
			plus_enable = false;
		}
		else
		{
			minusball->setVisible(false);
			plusball->setVisible(true);
			plus_enable = true;
		}
	}

	if (keyCode == EventKeyboard::KeyCode::KEY_R)
	{
		plusball->setPosition(Point(origin.x + 40 , 60 + origin.y));
		currentposition = plusball->getPosition();
		minusball->setPosition(currentposition);
	}

}

void HelloWorldLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
	{
		rightpress = false;
	}

	if (keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
	{
		leftpress = false;
	}
}


//---------------------------------------------------------------------------
bool HelloWorldScene::init()
{
    if(Scene::init()) return false;
    return true;
};