#include "Ball.h"
#include "Ground.h"
#include "PullPush.h"
using namespace cocos2d;

bool Ball::init(float radius)
{
    sprite = Sprite::create("plus_ball.png");
    float size = sprite->getContentSize().width;
    float scale = radius / size * 2;
	ball_scale = scale;
    sprite->setScale(scale);
    setPosition(Point::ZERO);
    addChild(sprite);

    velocity = Point::ZERO;
    acceleration = Point::ZERO;
    mass = 0.01f;

    setContentSize(sprite->getContentSize() * scale);
    setattached(0);

    return true;
}

Ball* Ball::create(float Radius)
{
    Ball* ret = new Ball();
    if((ret != nullptr) && ret->init(Radius))
        ret->autorelease();
    else
        CC_SAFE_RELEASE_NULL(ret);
	ret->radius = Radius;
    return ret;
}

float Ball::getRadius() const
{
    return radius;
}

int Ball::getattached() const
{
    return attached;
}

int Ball::setattached(int a)
{
    return attached = a;
}

void Ball::initForce()
{
    acceleration = Point::ZERO;
}

void Ball::applyForce(const Point& force)
{
    acceleration += force / mass;
}

void Ball::resetVelocity()
{
    velocity = Point::ZERO;
}

void Ball::XresetVelocity()
{
    velocity.x = 0;
}

void Ball::YresetVelocity()
{
    velocity.y = 0;
}

void Ball::sameVelocity(int direction)
{
    if (direction == 1)
    {

        velocity.x = 200.0f;
    }

    if (direction == -1)
    {
        velocity.x = -200.0f;
    }
}

void Ball::updatePhysics(Ball* ball, CCArray* groundarray, CCArray* pullpusharray, float deltaTime)
{
    Point position = getPosition();
    oldposition = getPosition();

    CCObject* obj1 = NULL;
    CCObject* obj2 = NULL;
    velocity += acceleration * deltaTime;
    ball -> setPosition(ball->getPosition() + velocity * deltaTime);

    CCARRAY_FOREACH(pullpusharray, obj1)
    {
        PullPush* pullpush = (PullPush*)obj1;

        if(getattached() == 0)
		   pushbarrier(ball, pullpush, (ball->getPosition()));
        else
        {										// 그냥 붙었을 경우를 아예 따로 처리해버림.
		    if(velocity.x > 0)						// 각도 바뀌는 경우
		    {
			   angle-=0.1;
		    }
            else if(velocity.x < 0)
            {
			   angle+=0.1;
		    }

		    position = anch + Point(circle_radius * cos(angle),circle_radius * sin(angle));
		    // 포지션을 고정시켜서 돌려버림
            ball->setPosition(position);
        }
    }

        

        CCARRAY_FOREACH(groundarray, obj2)
        {
            Ground* ground = (Ground*)obj2;
            Point oldvelocity = velocity;
		    ColideBarrier(ball, ground, (ball->getPosition()));
        }

}

void Ball::ColideBarrier(Ball* ball, const Ground* ground, Point pos)
{
    Point temp_ballposition = getPosition();
    Point temp_groundposition = ground->getPosition();
    float temp_ballsize = getContentSize().height;
    float temp_Xgroundsize = ground->getContentSize().width;
    float temp_Ygroundsize = ground->getContentSize().height;


        if ((0 <= (pos.x - temp_groundposition.x)) && ((pos.x - temp_groundposition.x) <= (ground->getContentSize().width)))
        {
            if (((pos.y - temp_groundposition.y) > 0) && ((pos.y - temp_groundposition.y) <= (temp_Ygroundsize + temp_ballsize/2)))
            {
                temp_ballposition = Point(pos.x,temp_groundposition.y + temp_Ygroundsize + temp_ballsize/2);
                velocity.y = 0;
                acceleration.y = 0;
                ball->setPosition(temp_ballposition);
            }
            else if (((pos.y - (temp_groundposition.y + temp_Ygroundsize) <= 0)) && (abs(pos.y - temp_groundposition.y) <= temp_ballsize/2))
            {
                temp_ballposition = Point(pos.x,temp_groundposition.y - temp_ballsize/2);
                velocity.y = 0;
                acceleration.y = 0;
                ball->setPosition(temp_ballposition);
            }
            else
                ball->setPosition(pos);                
        }
        else if(((pos.x - temp_groundposition.x) <= 0) || ((pos.x - temp_groundposition.x) >= temp_Xgroundsize))
        {
            if ((-temp_ballsize/2 <= (pos.y - temp_groundposition.y)) && ((pos.y - temp_groundposition.y) <= (temp_Ygroundsize + temp_ballsize/2)))
            {
                if (((pos.x - temp_groundposition.x) >= 0) && ((pos.x - temp_groundposition.x) <= (temp_Xgroundsize + temp_ballsize/2)))
                {
                    temp_ballposition = Point(temp_groundposition.x + temp_Xgroundsize + temp_ballsize/2 ,pos.y);
                    velocity.x = 0;
                    acceleration.x = 0;
                    ball->setPosition(temp_ballposition);
                }
                else if (((pos.x - temp_groundposition.x) < 0) && (abs(pos.x- temp_groundposition.x) <= temp_ballsize/2))
                {
                    temp_ballposition = Point(temp_groundposition.x - temp_ballsize/2 ,pos.y);
                    velocity.x = 0;
                    acceleration.x = 0;
                    ball->setPosition(temp_ballposition);
                }
                else
                    ball->setPosition(pos);
            }
            else
                ball->setPosition(pos);
            
        }
}

void Ball::pushforce(Ball* ball, CCArray* pullpusharray)
{
    CCObject* obj1 = NULL;

    CCARRAY_FOREACH(pullpusharray, obj1)
    {
        PullPush* pullpush = (PullPush*)obj1;

        Point ball_position = getPosition();
        Point pullpush_position = pullpush->getPosition();
	    float temp_ballsize = this->radius;
	    float temp_pullpushsize = pullpush->getrangesize().height;
        float temp_rangesize = pullpush->getrangesize().height;
        float force_x;
        float force_y;

	    float force;
	    float delta_x;
	    float delta_y;
	    float distance;

	    delta_x = pullpush_position.x-ball_position.x;
	    delta_y = pullpush_position.y-ball_position.y;
	    distance = ccpDistance(pullpush_position, ball_position);

        if (distance < temp_rangesize/2)
        {
		    force = float(pullpush->getState()) * 140000.0f / pow(distance, 2);
		    applyForce(Point(force * (delta_x/distance), force * (delta_y/distance)));
        }
    }
    
}

void Ball::pushbarrier(Ball* ball, const PullPush* pullpush, Point pos)
{
    Point temp_ballposition = pos;
    Point temp_pullpushposition = pullpush->getPosition();
    float temp_ballsize = getContentSize().height;
    float temp_pullpushsize = pullpush->getContentSize().height;
    float distance = ccpDistance(temp_ballposition, temp_pullpushposition);
    float ratio;
    float bitbyeon;
    float temp_x;
    float temp_y;

    //닮음비를 이용해서 공을 되돌리려고 했으나 잘 안됨
    if(((temp_ballsize/2) + (temp_pullpushsize/2)) >= distance)
    {
		// 붙을 경우에 attached의 상태만 바꾸어줌. 아 물론 나머지 정보도 긁어옴.
        if (pullpush->getState() == 1)     
            setattached(1);

        velocity = Point::ZERO;
        acceleration = Point::ZERO;
		angle = atan2f(this->getPositionY() - pullpush->getPositionY(), this->getPositionX() - pullpush->getPositionX());
		//angle = CC_RADIANS_TO_DEGREES(angle);
		anch = pullpush->getPosition();
		circle_radius = pullpush->getRadius()*pullpush->getScale() + this->getRadius()*this->getScale();
    }

}